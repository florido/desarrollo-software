# Resolver la ecuación cuadrática ax^2+bx+c=0

#Importar librería de matemática compleja
import cmath

a = 1
b = 5
c = 6

# Calculamos el discriminante

d= (b**2)- (4*a*c)

#Calculamos las dos soluciones
sol1 = (-b+ cmath.sqrt(d))/(2*a)

sol2= (-b - cmath.sqrt(d))/(2*a)

print('Las soluciones son {0} y {1}'.format(sol1,sol2))